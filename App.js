import AppContainer from './src/containers/AppContainer';
import React from 'react';
import { Provider } from 'react-redux';
import { Animated, Text, View,Easing } from 'react-native';
import Splash from './splash';

export default class App extends React.Component {
  render() {
    return (
        <AppContainer />
    )
  }
}