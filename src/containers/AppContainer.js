import React, { Component } from 'react';
import { View, Text, YellowBox, Linking, PixelRatio, TouchableHighlight, Image, Button, TouchableOpacity } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { createMaterialTopTabNavigator, TabNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation';
import { Column as Col, Row } from 'react-native-flexbox-grid';
import { TAB_NAVIGATION_MENU, TAB_BAR_OPTIONS } from '../constants';
import App from '../../App';
import { connect } from 'react-redux';
import Signup from '../components/Pages/Signup';
import Login from '../components/Pages/Login';
import Main from '../components/Pages/Main';
import Devices from '../components/Pages/Devices';
import Scenes from '../components/Pages/Scenes';
import Splash from '../components/Pages/Splash';
import HeaderScreen from '../components/Pages/HeaderScreen';
import DrawerScreen from '../components/Pages/DrawerScreen';
import CardDetails from '../components/Pages/CardDetailsScreen';


const HeaderScreen1 = createStackNavigator({
  HeaderScreen: {
    screen: Main,
    navigationOptions: ({ navigation }) => ({
      headerStyle: { backgroundColor: '#223E4A', elevation: 0, shadowOpacity: 0 },
      headerTitleStyle: { width: '100%' },
      headerLeft: <TouchableOpacity onPress={() => navigation.toggleDrawer()}><Image source={require('../utils/images/white_menu_icon.png')} style={{ width: 40, height: 40, margin: 10 }} /></TouchableOpacity>,
      title: "Ginny",
      headerTintColor: 'white',
    })
  }
})

const MainScreenDrawerNavigator = createDrawerNavigator({
  Tabs: {
    screen: HeaderScreen1,
  },
}, {
    contentComponent: props => <DrawerScreen {...props} />
  });

const RootNavigation = createStackNavigator({
  splash : {
    screen : Splash,
    navigationOptions :{
      header : null
    }
   },
  Login: {
    screen: Login,
    navigationOptions: {
      header: null
    }
  },
  Main: {
    screen: MainScreenDrawerNavigator,
    navigationOptions: {
      header: null
    },
    lazy: false
  },
  CardDetails: {
    screen: CardDetails,
    navigationOptions: {
      header: null
    }

  },
  DrawerScreen: {
    screen: DrawerScreen,
    navigationOptions: {
      header: null
    }
  },
  Signup: {
    screen: Signup,
    navigationOptions: {
      header: null
    }
  }
});

export default class AppContainer extends Component {
  constructor(props) {
    super(props);

  }
  render() {
    return (
      <RootNavigation />
    );
  }
}