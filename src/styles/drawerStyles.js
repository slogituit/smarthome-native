import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const styles = StyleSheet.create({
    drawerContainer : {
        height: hp('17%'),
        width:'100%',
        backgroundColor: '#223E4A'
    },
    containerAlign : {
        flex:1,
        flexDirection:'column',
        alignItems:'center',  
        justifyContent:'center',  
       
    },  
    containerText : {
        color:'white',
        fontWeight: 'bold',
        fontSize:25,
        height:hp('5%'),
        width:hp('30%'),
        //marginLeft:'50%',
    },
    userText : {
        color:'white',
        fontWeight: 'bold',
        fontSize: 20,
        height:hp('4%'),
        width:hp('30%'),
        //marginLeft:'40%'
    },
    drawerListView : {
        backgroundColor: 'white'
    },
    listContainer : {
        padding: 20,
        backgroundColor: 'white',
        flexDirection:'row',
        flexWrap:'wrap',
        alignItems: 'center',
    },
    text: {
        marginLeft: 20,
        color: 'black',
        fontWeight: 'bold',
        alignItems: 'center',
        fontSize: 15,
        height:hp('4%'),
        width:'60%'
    }    
});