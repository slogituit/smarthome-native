import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const styles = StyleSheet.create({
  signupBackground1 :{
        flex:1,
        backgroundColor : "#1d5e81" ,
        height : '100%',
        alignItems:'center'
    },
    alignCenter: {       
      
      flex : 1,
      flexDirection : 'row',
       alignItems:'center',
       justifyContent: 'center',
       marginTop : '20%'
   },
    LoginInputStyle : {
        height: hp('10%'),
        width: wp('20%'), 
    },
    signupBox: { 
        // backgroundColor: "white",
        // borderBottomLeftRadius:8,
        // borderTopRightRadius:8,
        // borderWidth: 2,
        // borderColor: '#000000',
        // height: hp('60%'),
        // width:wp('90%'),
        // bottom : '20%',
        // margin : '5%'

            
        backgroundColor: "white",
        borderBottomLeftRadius: 8,
        borderTopRightRadius: 8,
        borderWidth: 2,
        borderColor: '#000000',
        height: hp('85%'),
        width: wp('85%'),
      },
      button: {
         alignSelf : 'center',
        // justifyContent:'center',
        // backgroundColor: '#1d5e81',
      
        // height: hp('8%'),
        // width: wp('80%'),
        // alignItems : 'center',
        // backgroundColor: '#151922',
        //padding: 10,
        height:hp('6%'),
        width:wp('60%'),
        
        // marginLeft : 15,
        // marginRight : 15
        // justifyContent : 'center'

        marginLeft : 2,
        marginRight : 2,
        marginTop: 4,
        alignItems: 'center',
        backgroundColor: '#223E4A',
        justifyContent : 'center',
      },
      buttonTextStyle : {
        backgroundColor : "white"

      },
      registerTextStyle : {
        // color:"white",
        // fontSize : hp('1.8%'),
        // marginTop:hp('1.7%'),
        // fontWeight:'bold',
        // alignItems:'center',
        // justifyContent: 'center'
        color: "#87ceeb",alignItems:'center',fontWeight:'bold'
      },
      signupBackgroundColor2 : {
        backgroundColor: "white"
      },
      imageStyle : {
       height : hp('100%'),
       width  : wp('100%')
      },
      textBoxBtnHolder:
    {
      position: 'relative',
      alignSelf: 'stretch',
      justifyContent: 'center'
    },
   
    textBox:
    {
      fontSize: 18,
      alignSelf: 'stretch',
      paddingRight: 45,
      paddingLeft: 8,
      paddingVertical: 0,
    },
   
    visibilityBtn:
    {
      position: 'absolute',
      right: 3,
      height: 40,
      width: 35,
      padding: 5
    },
   
    btnImage:
    {
      resizeMode: 'contain',
      height: '100%',
      width: '100%',
      top:'5%'
    }
})