import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export const styles = StyleSheet.create({
    LoginBackground1: {
        flex: 1,
        backgroundColor: "#223E4A",
        height: hp('40%'),
    },
    LoginInputStyle: {
        flex: 1,
        height: hp('30%'),
        width: wp('50%'),
    },
    alignCenter: {       
      
       flex : 1,
       flexDirection : 'column',
        alignItems:'center',
        justifyContent: 'center'
          // marginTop : '27.5%'
    },
    loginBox: {    
    
        backgroundColor: "white",
        borderBottomLeftRadius: 8,
        borderTopRightRadius: 8,
        borderWidth: 2,
        borderColor: '#000000',
        height: hp('45%'),
        width: wp('85%'),
      
        


        
        // alignItems: 'center'
        // justifyContent: 'center',
        //alignItems: 'center', 
        // flex: 1,
    },
    button: {
         alignItems: 'center',
        backgroundColor: '#223E4A',
        justifyContent : 'center',
        //  padding: '6%',
        height: hp('6%'),
        // width: wp('30%'),
        // marginLeft: wp('9%'),
        marginTop: wp('18%'),
        marginBottom: wp('4%')
    },
     container:
    {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      paddingVertical: '6%',
    },
   
    textBoxBtnHolder:
    {
      position: 'relative',
      alignSelf: 'stretch',
      justifyContent: 'center'
    },
   
    textBox:
    {
      fontSize: 18,
      alignSelf: 'stretch',
      paddingRight: 45,
      paddingLeft: 8,
      paddingVertical: 0,
    },
   
    visibilityBtn:
    {
      position: 'absolute',
      right: 3,
      height: 40,
      width: 35,
      padding: 5
    },
   
    btnImage:
    {
      resizeMode: 'contain',
      height: '100%',
      width: '100%',
      top:'5%'
    }
});
