import React, { Component } from 'react';
import { View, Text, TextInput, Button, StyleSheet, TouchableOpacity, ScrollView, Image, AsyncStorage } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import { Column as Col, Row } from 'react-native-flexbox-grid';
import { styles } from '../../styles/loginStyles';
import { DrawerScreen } from '../Pages/DrawerScreen';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import PasswordInputText from 'react-native-hide-show-password-input';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

//https://ai.logituit.com/homeautomation  --> logituit server url

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hidePassword: true,
      usernameError: '',
      passwordError: '',
      username: '',
      password: '',
    }
    this.navigateSignup = this.navigateSignup.bind(this);
    this.onFocus = this.onFocus.bind(this);
  }
  managePasswordVisibility = () => {
    this.setState({ hidePassword: !this.state.hidePassword });
  }

  // retrieveData() {
  //   let that = this;
  //   AsyncStorage.getItem('loginResponse', (err, result) => {
  //     if (result) {
  //       result = JSON.parse(result);
  //       that.navigateToCardDetailsScreen(result);
  //       if (result.data.code === 0 && result.status === 200) {
  //         that.setState({ username: '', password: "" });
  //       }
  //       //console.log("result in login get asyncdata::::::",result);
  //       that.props.navigation.navigate('Main');
  //     }
  //   });
  // }

  // componentDidMount() {
  //   let that = this;
  //   that.retrieveData();
  //   // this.didFocusListener = this.props.navigation.addListener(
  //   //   'didFocus',
  //   //   () => { this.retrieveData() },
  //   // );
  // }

  navigateToCardDetailsScreen(name) {
    let that = this;
    that.props.navigation.navigate('DrawerScreen', { name: name });

  }

  navigateSignup(ref) {
    let that = this;
    let data = {
      "email": this.state.username,
      "password": this.state.password
    }

    if (ref === "LOGIN") {
      const err = this.validation();

     
      let that = this;

      axios.post("https://ai.logituit.com/homeautomation/register/login", data)
        .then(function (response) {
          let myObjStr = JSON.stringify(response);
          if ((response.data.status === 200) && (response.data.message)) {
            that.navigateToCardDetailsScreen(response);
            if (myObjStr) {
              AsyncStorage.setItem('loginResponsedata', "loggedIn");
              AsyncStorage.setItem('loginResponse', myObjStr);
              AsyncStorage.setItem('loginEmail', that.state.username);
              AsyncStorage.setItem('loginPass', that.state.password);
            }
        // const userId = AsyncStorage.getItem('loginResponsedata');
        // var datalogin= JSON.stringify(userId);
        // console.log("datalogin=====",datalogin);
         that.props.navigation.navigate('Main');
          }
          else {
            Toast.show('Email or Password incorrect');
          }
        })
    } else {
      that.props.navigation.navigate('Signup');
    }
  }

  validation = () => {
    let isError = false;
    const errors = {};
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.state.username.length === 0) {
      this.setState(() => ({ usernameError: "Username required." }));
      isError = true;
    }
    else if (reg.test(this.state.username) === false) {
      this.setState(() => ({ usernameError: "Username invalid." }));
      isError = true;
    }
    else {
      this.setState(() => ({ usernameError: null }));
    }
    if (this.state.password.length < 4) {
      this.setState(() => ({ passwordError: "Minimum 4 character required." }));
      isError = true;
    } else {
      this.setState(() => ({ passwordError: null }));
    }

    return isError;
  }
  onFocus(a) {
    if (a === 'e') {
      this.setState({ usernameError: '' });
    }
    if (a === 'p') {
      this.setState({ passwordError: '' });
    }
  }


  render() {
    return (
      <View style={{ height: hp('100%'), backgroundColor: '#151922' }}>

        <View style={styles.alignCenter}>
          <View style={styles.loginBox}>
            <View style={{ marginLeft: '7%', marginRight: '7%' }}>
              <TextField
                label='Email'
                textColor="#1d5e81"
                fontSize={20}
                labelFontSize={15}
                tintColor='#1d5e81'
                autoFocus={true}
                ref="Email"
                onFocus={() => this.onFocus("e")}
                onChangeText={(text) => this.setState({ username: text })}
                //onChangeText={(text) => this.change({ username: text })}
                value={this.state.username}
                floatingLabelFixed
              />
              {!!this.state.usernameError && (
                <Text style={{ color: 'red' }}>{this.state.usernameError}</Text>
              )}
            </View>
            <View style={{ marginLeft: '7%', marginRight: '7%' }}>
              <View style={styles.container}>
                <View style={styles.textBoxBtnHolder}>
                  <TextField label='Password'
                    textColor="#87ceeb"
                    fontSize={20}
                    onFocus={() => this.onFocus("p")}
                    ref="password"
                    labelFontSize={15}
                    onChangeText={(text) => this.setState({ password: text })}
                    tintColor='#1d5e81' underlineColorAndroid="transparent" secureTextEntry={this.state.hidePassword} style={styles.textBox} />
                  {!!this.state.passwordError && (
                    <Text style={{ color: 'red' }}>{this.state.passwordError}</Text>
                  )}
                  <TouchableOpacity activeOpacity={0.8} style={styles.visibilityBtn} onPress={this.managePasswordVisibility}>
                    <Image source={(this.state.hidePassword) ? require('./hide.png') : require('./show.png')} style={styles.btnImage} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <Row >
              <Col sm={1} md={1} lg={1}>
              </Col>
              <Col sm={4} md={4} lg={4}>
                <TouchableOpacity
                  ref={'SIGNUP'}
                  style={styles.button}
                  onPress={() => this.navigateSignup("SIGNUP")}
                >
                  <Text style={{ color: "#ffffff", fontSize: hp('1.8%'), fontWeight: 'bold' }}> SIGN UP </Text>
                </TouchableOpacity>
              </Col>
              <Col sm={1} md={1} lg={1}>
              </Col>
              <Col sm={4} md={4} lg={4}>
                <TouchableOpacity
                  ref={'LOGIN'}
                  style={styles.button}
                  onPress={() => this.navigateSignup("LOGIN")}
                >
                  <Text style={{ color: "#ffffff", fontSize: hp('1.8%'), fontWeight: 'bold' }}>LOGIN </Text>
                </TouchableOpacity>
              </Col>
              <Col sm={1} md={1} lg={1}>
              </Col>
            </Row>
          </View>
        </View>
      </View>
    );
  }
}