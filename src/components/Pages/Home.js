import React, { Component } from 'react';
import AppContainer from '../../containers/AppContainer.js'

const backgroundColor = "#1d5e81";
export default class Home extends Component{

static navigationOptions = ({navigation}) => {
  let drawerLabel = 'Home';
  let drawerIcon = () => (
    <Image
    source={require('../../utils/images/homeIcon.png')}
    style={{height:26,width:26,tintColor:backgroundColor}}
    />
  )
     return{drawerLabel,drawerIcon} ;
}
    render() {
        return(
            <AppContainer {...this.props}/>
        
        )
    }
}