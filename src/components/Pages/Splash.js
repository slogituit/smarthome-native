import React, { Component } from 'react';
import { Animated, StyleSheet, Text, View,Easing ,AsyncStorage} from 'react-native';
 export default class Splash extends Component {
      constructor(props) {
        super(props);
        this.state = {
          loginEmail:'',
          loginPass:''
        }
      }

       componentDidMount() {
         this.retrieveData();
        }
       retrieveData(){
            let that = this;
            let value2,value1;
            setTimeout(function(){
              try {
                AsyncStorage.getItem('loginEmail', (err, value) => {
                  if (value !== null) {
                  that.setState ({
                    loginEmail: value
                    })
                  }
                });
                } catch (error) {
                console.log('Error: ',error);
              }
            },1000);
            setTimeout(function(){
              try {
                AsyncStorage.getItem('loginPass', (err, value) => {
                  if (value !== null) {
                  that.setState ({
                    loginPass: value
                    })
                  }
                });
             } catch (error) {
                console.log('Error: ',error);
              }
            },1000);
            setTimeout(function(){
            try {
                AsyncStorage.getItem('loginResponsedata', (err, result) => {
                  console.log("in splash screen if asyncstorage********",result);
                    if (result !== null) {
                        if(result == "loggedIn"){
                          console.log("result in splash:::::::::",result);
                          that.props.navigation.navigate('Main');
                          // let data = {
                          //     email: that.state.loginEmail,
                          //     password :that.state.loginPass
                          //   }
                          //     let callback = {};
                          //     callback.successCallback = function () {
                          //       that.props.navigation.navigate('Main'); 
                          //     }
                          //     callback.failureCallback = function () {
                          //     }
                          //     that.props.screenProps.login(data, callback);
                            } else {
                              that.props.navigation.navigate('Login');
                          }
                        } else {
                          that.props.navigation.navigate('Login');
                      }
                          });
                    } catch (error) {
                      console.log("Error retrieving data",error);; 
                    }
                  },3000);
      }
      
     render(){
        return (
            <View style={styles.container}>
               <Animated.Image
            style={{
              width: 200,
              height: 200,
               }}
              source={require('../../../ginny.png')} />
              <Text style={styles.text}>Ginny</Text>
            </View>
          )
     }
 }  
 const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor:'#223E4A',
    },
    text:{
      fontWeight:'bold',
      color:'#fff',
      fontSize:20,
      paddingTop:'5%'
    }
  })