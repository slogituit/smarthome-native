
import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    Button,
    StyleSheet,
    TouchableOpacity,
    ScrollView, Image,
    Picker,
    AsyncStorage,
    KeyboardAvoidingView

} from 'react-native';
import axios from 'axios';
import { TextField } from 'react-native-material-textfield';
import { styles } from '../../styles/signupStyles';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Column as Col, Row } from 'react-native-flexbox-grid';
import DatePicker from 'react-native-datepicker'
import RadioButton from 'radio-button-react-native';


var radio_props = [
    { label: 'Male', value: "M" },
    { label: 'Female', value: "F" }
];
export default class Signup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            hidePassword: true,
            firstName: '',
            firstNameError: '',
            lastName: '',
            lastNameError: '',
            email: '',
            emailError: '',
            phoneNo: '',
            phoneNoError: '',
            password: '',
            passwordError: '',
            birthDate: '',
            gender: '',
            value: "M",


        }
        this.navigateToLogin = this.navigateToLogin.bind(this);
        this.onFocus = this.onFocus.bind(this);


    }
    handleOnPress(value) {
        this.setState({ value: value })
    }

    managePasswordVisibility = () => {
        this.setState({ hidePassword: !this.state.hidePassword });
    }

    validation = () => {
        let isError = false;
        const errors = {};

        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (this.state.email.length === 0) {
            this.setState(() => ({ emailError: "email required." }));
            isError = true;
        } else if (reg.test(this.state.email) === false) {
            this.setState(() => ({ emailError: "email invalid." }));
            isError = true;
        } else {
            this.setState(() => ({ emailError: null }));
        }

        if (this.state.password.length < 4) {
            this.setState(() => ({ passwordError: "Minimum 4 character required." }));
            isError = true;
        } else {
            this.setState(() => ({ passwordError: null }));
        }

        if (this.state.phoneNo.length < 10 || isNaN(this.state.phoneNo) || this.state.phoneNo.length > 15) {
            this.setState(() => ({ phoneNoError: "Phone Number is required." }));
            isError = true;
        }
        else {
            this.setState(() => ({ phoneNoError: null }));
        }        
        if (this.state.firstName.length === 0) {
            this.setState(() => ({ firstNameError: "First Name required." }));
            isError = true;
        } else {
            this.setState(() => ({ firstNameError: null }));
        }
        if (this.state.lastName.length === 0) {
            this.setState(() => ({ lastNameError: "Last Name required." }));
            isError = true;
        } else {
            this.setState(() => ({ lastNameError: null }));
        }

        return isError;
    }

    navigateToLogin() {
        let isError = false;
        let errors = {};
        let that = this;
        let callback = {};

        const err = this.validation();
        console.log("err : ", err);
        if (!err) {
            let data = {
                'firstName': this.state.firstName,
                'lastName': this.state.lastName,
                'email': this.state.email,
                'phoneNo': this.state.phoneNo,
                'password': this.state.password,
                'birthDate': this.state.birthDate,
                'gender': this.state.gender,
            }

            axios.post("https://ai.logituit.com/homeautomation/register/", data)
                .then(function (response) {
                    console.log("response : ", response);
                    let myObjStr = JSON.stringify(response);
                    if (myObjStr) {
                        AsyncStorage.setItem('signupResponse', myObjStr);
                    }
                    if (response.data.status === 200) {
                        that.props.navigation.navigate('Login');
                    }
                    else {
                        Toast.show('Data is incorrect');
                    }
                })
        }
    }
    onFocus(e) {
        console.log("in onfocus::::", e);
        if (e === 'a') {
            this.setState({ firstNameError: '' });
        }
        if (e === 'b') {
            this.setState({ lastNameError: '' });
        }
        if (e === 'c') {
            this.setState({ emailError: '' });
        }
        if (e === 'd') {
            this.setState({ phoneNoError: '' });
        }
        if (e === 'e') {
            this.setState({ passwordError: '' });
        }
    }
    render() {
        return (
            //<KeyboardAvoidingView w style={{ height: hp('100%'), backgroundColor: '#151922' }} behavior="padding">

            <View style={{ height: hp('100%') }}>
                <ScrollView>
                    <View style={styles.alignCenter}>
                        <View style={styles.signupBox}>
                            <View class="classname" style={{ marginLeft: 15, marginRight: 15, height: hp('9%') }}>
                                <TextField
                                    label='First Name'
                                    textColor='#1d5e81'
                                    tintColor='#1d5e81'
                                    autoFocus={true}
                                    // error = {this.state.firstNameError}
                                    // errorColor = "red"
                                    fontSize={16}
                                    onFocus={() => this.onFocus("a")}
                                    labelFontSize={15}
                                    tintColor='#1d5e81'
                                    isRestricted={true}
                                    value={this.state.firstName}
                                    onChangeText={(text) => this.setState({ firstName: text })}
                                />
                                {!!this.state.firstNameError && (
                                    <Text style={{ color: 'red' }}>{this.state.firstNameError}</Text>
                                )}
                            </View>

                            <View style={{ marginLeft: 15, marginRight: 15, height: hp('9%') }}>
                                <TextField
                                    labelPaddingb={4}
                                    label='Last Name'
                                    textColor="#1d5e81"
                                    fontSize={16}
                                    labelFontSize={15}
                                    tintColor='#1d5e81'
                                    value={this.state.lastName}
                                    onFocus={() => this.onFocus("b")}
                                    //     error = {this.state.lastNameError}
                                    //    errorColor = {red}
                                    onChangeText={(text) => this.setState({ lastName: text })}
                                />
                                {!!this.state.lastNameError && (
                                    <Text style={{ color: 'red' }}>{this.state.lastNameError}</Text>
                                )}

                            </View>

                            <View style={{ marginLeft: 15, marginRight: 15, height: hp('9%') }}>
                                <TextField
                                    label='Email'
                                    textColor="#1d5e81"
                                    fontSize={16}
                                    labelFontSize={15}
                                    tintColor='#1d5e81'
                                    value={this.state.email}
                                    onFocus={() => this.onFocus("c")}
                                    //    error = {this.state.email}
                                    //    errorColor = {red}
                                    onChangeText={(text) => this.setState({ email: text })}
                                />
                                {!!this.state.emailError && (
                                    <Text style={{ color: 'red' }}>{this.state.emailError}</Text>
                                )}
                            </View>
                            <View style={{ marginLeft: 15, marginRight: 15, height: hp('9%') }}>
                                <TextField
                                    label='Phone Number'

                                    textColor="#1d5e81"
                                    fontSize={16}
                                    labelFontSize={15}
                                    tintColor='#1d5e81'
                                    value={this.state.phoneNo}
                                    onFocus={() => this.onFocus("d")}
                                    //     error = {this.state.phoneNoError}
                                    //    errorColor = {red}
                                    onChangeText={(text) => this.setState({ phoneNo: text })}
                                />
                                {!!this.state.phoneNoError && (
                                    <Text style={{ color: 'red' }}>{this.state.phoneNoError}</Text>
                                )}
                            </View>
                            <View style={{ marginLeft: 15, marginRight: 15, height: hp('9%') }}>
                                <View style={styles.container}>
                                    <View style={styles.textBoxBtnHolder}>
                                        <TextField label='Password'
                                            textColor='#1d5e81'
                                            fontSize={16}
                                            labelFontSize={15}
                                            onFocus={() => this.onFocus("e")}
                                            tintColor='#1d5e81' underlineColorAndroid="transparent" secureTextEntry={this.state.hidePassword} style={styles.textBox}
                                            value={this.state.password}
                                            // error = {this.state.passwordError}
                                            // errorColor = {red}
                                            onChangeText={(text) => this.setState({ password: text })}
                                        />
                                        {!!this.state.passwordError && (
                                            <Text style={{ color: 'red' }}>{this.state.passwordError}</Text>
                                        )}
                                        <TouchableOpacity activeOpacity={0.8} style={styles.visibilityBtn} onPress={this.managePasswordVisibility}>
                                            <Image source={(this.state.hidePassword) ? require('./hide.png') : require('./show.png')} style={styles.btnImage} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>

                            <View style={{marginTop:5, marginLeft: 15, marginRight: 15, height: hp('9%') }}>
                            	<Text style = {{fontSize:15}}>BirthDate</Text>
                                <DatePicker
					                    style={{width: 200}}
					                    date={this.state.birtDate}
					                    mode="date"
					                    placeholder="select date"
					                    format="MM-DD-YYYY"
					                    minDate="2016-05-01"
					                    maxDate={new Date()}
					                    confirmBtnText="Confirm"
					                    cancelBtnText="Cancel"
					                    onDateChange={(date) => {this.setState({birtDate: date})}}
                					/>
                            </View>
                            <View style={{ marginLeft: 15, marginRight: 15, marginTop: 15, height: hp('9%') }}>

                                <Row style={{ marginTop: 15 }}>
                                    <Col sm={6} md={6} lg={6}>
                                        <RadioButton
                                            currentValue={this.state.gender}
                                            innerCircleColor="#223E4A"
                                            value={"M"}
                                            onPress={(value) => { this.setState({ gender: value }) }}

                                        // onPress={this.handleOnPress.bind(this)}

                                        >
                                            <Text style={{ marginLeft: 10 }}>Male</Text>
                                        </RadioButton>
                                    </Col>
                                    <Col sm={6} md={6} lg={6}>
                                        <RadioButton currentValue={this.state.gender}
                                            innerCircleColor="#223E4A"
                                            value={"F"} onPress={(value) => { this.setState({ gender: value }) }}>
                                            <Text style={{ marginLeft: 10 }}>Female</Text>
                                        </RadioButton>
                                    </Col>
                                </Row>
                            </View>
                            <TouchableOpacity
                                style={styles.button}
                                onPress={() => this.navigateToLogin()}
                            >
                                <Text style={styles.registerTextStyle}> REGISTER </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>

            // </KeyboardAvoidingView>


        )
    }


}
