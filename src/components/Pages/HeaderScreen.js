import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Button , Icon} from 'native-base'; 


export default class HeaderScreen extends Component {
    static navigationOption = ({ navigation }) =>{
        return {
            headerLeft: (
                <Button>
                <Icon 
                    name = "menu"
                    style = {{fontSize : 30,color:'white'}}/>
            </Button>
            )
            
        }
    }
    render(){
        return(
            <Text>HeaderScreen</Text>
        )
    }
}