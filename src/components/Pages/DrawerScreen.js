import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, AsyncStorage, Image } from 'react-native';
import { styles } from '../../styles/drawerStyles';
import { connect } from 'react-redux';
import LineDiveder  from '../LineDiveder';

export default class DrawerScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            result: [],
        }
    }

    removeData = () => {
        AsyncStorage.removeItem('loginResponsedata');
        AsyncStorage.removeItem('loginResponse');
        AsyncStorage.removeItem('loginEmail');
        AsyncStorage.removeItem('loginPass');
    }
    retrieveData() {
        let that = this;
        AsyncStorage.getItem('loginResponse', (err, result) => {
            console.log("result in detailscreen ::::::", result);  
            // if(result == null){
            //    goBack();
            // } 
            if (result) {
                result = JSON.parse(result);
                that.setState(() => ({ result: result }));                    
            }
        });
    }

    componentDidMount() {
        let that = this;
        that.retrieveData();
    }

    navigateToLogin = () => {
        let that = this;
        that.removeData();
        that.props.navigation.navigate('Login')
    }

    render() {
        console.log("in drawer screen================");
        const { goBack } = this.props.navigation;
        //console.log("this.props.navigation.state.params",this.props.navigation.state.params);
        if(this.state.result.data && this.state.result.data.message){
        return (
            <View style={StyleSheet.absoluteFill}>
                <View style={styles.drawerContainer}>
                    <View style={styles.containerAlign}>
                        <Text style={styles.containerText} > Welcome</Text>
                        {this.state.result.data && this.state.result.data.message &&
                            <Text style={styles.userText} > {this.state.result.data.message.firstName} </Text>
                        }
                    </View>
                </View>
                <LineDiveder/>
                <View style={{ backgroundColor: '#223E4A', height: '100%' }}>
                    <TouchableOpacity style={{ padding: 20, backgroundColor: '#223E4A', flexDirection: 'row', flexWrap: 'wrap' }} onPress={() => goBack()}>
                        <Image source={require('../../utils/images/home.png')} style={{ height: 25, width: 25 }} />
                        <Text style={{ marginLeft: 20, color: '#ffffff', fontWeight: 'bold', fontSize: 15, height: 20, width: '30%' }}> Home</Text>
                    </TouchableOpacity >
                    <TouchableOpacity style={{ padding: 20, backgroundColor: '#223E4A', flexDirection: 'row', flexWrap: 'wrap', }} onPress={() => this.navigateToLogin()}>
                        <Image source={require('../../utils/images/logout.png')} style={{ height: 25, width: 25 }} />
                        <Text style={{ marginLeft: 20, color: '#ffffff', fontWeight: 'bold', fontSize: 15, height: 22, width: '60%' }}> Logout </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    else{
        return(
            <View>
            </View>
        )
    }
    }
}