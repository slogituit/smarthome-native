import React, { Component } from 'react';
import {
    View, Text, Alert, TextInput, Button, StyleSheet, BackHandler, TouchableHighlight, Image, TouchableOpacity, ScrollView
} from 'react-native';
import { Column as Col, Row } from 'react-native-flexbox-grid';
import AppContainer from '../../containers/AppContainer.js';
import ImageLoad from 'react-native-image-placeholder';
import axios from 'axios';

export default class Devices extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searching: false,
            resData: [],
            resDataresult: [],
            Data: [],
        }
        this.navigateToCardDetailsScreen = this.navigateToCardDetailsScreen.bind(this);
    }

    navigateToCardDetailsScreen(name, onIcon, offIcon) {
        let that = this;
        that.props.navigation.navigate('CardDetails', { title: name, onIcon: onIcon, offIcon: offIcon });
    }

    devices() {
        let that = this;
        let newdata = [];
        let devicedata = [];
        let devices = [];
        if (that.state.resData && that.state.resData.data) {
            devicedata = that.state.resData.data;
            devicedata.map(function (item, i) {
                devices = item.rooms[0].appliances;
            });
            devices.map(function (item, i) {
                if (item.applianceType === "scene") {
                    newdata.push(item);
                }
            });
            that.setState({ resDataresult: newdata });
        }
    }

    componentDidMount() {
        let that = this;
        axios.get("https://ai.logituit.com/homeautomation/user/getUserKnx")
            .then(function (response) {
                that.setState({ resData: response });
                that.devices();
            })
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }
    handleBackPress = () => {

        Alert.alert(
            'Exit App',
            'Exiting the application?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            },], {
                cancelable: true
            }
        )
        return true;
    }

    getAppliances(data) {
        let that = this;
        return data.map(function (item, i) {
            let image = { uri: (item.icon) }
            return (
                <Col sm={6} md={6} lg={6} key={i}>
                    <View style={{ flex: 1, alignSelf: 'stretch' }} >
                        <TouchableOpacity onPress={() => that.navigateToCardDetailsScreen(item.name, item.onIcon, item.offIcon)} style={{ height: 150, width: '100%', flex: 1, justifyContent: 'center', backgroundColor: '#223E4A', borderBottomWidth: 1, borderBottomRightRadius: 20, borderBottomLeftRadius: 20, borderBottomColor: '#808080' }}>
                            <ImageLoad
                                source={image}
                                style={{ alignSelf: 'center', height: '34%', width: '25%' }} resizeMode="cover"
                                placeholderSource={require("../../utils/images/placeholder.jpg")}
                                placeholderStyle={{ width: '25%', height: '34%' }} />
                            <Text style={{ alignSelf: 'center', color: '#fff', fontSize: 16, padding: '4%' }}>{item.name}</Text>
                        </TouchableOpacity>
                    </View>
                </Col>
            )
        })
    }

    render() {
        if (this.state.resDataresult.length) {
            let data = this.state.resDataresult;
            return (
                <View style={{ height: '100%', width: '100%', backgroundColor: '#223E4A' }}>
                    <View style={{ backgroundColor: '#223E4A' }}>
                        <ScrollView>
                            <View>
                                <View style={{ backgroundColor: '#223E4A' }}>
                                    <View style={{ top: 0, bottom: 0 }}>
                                        <Row>
                                            {this.getAppliances(data)}
                                        </Row>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </View>
            )
        }
        else {
            return (
                <View>
                    <Text> Loading ...</Text>
                </View>
            )
        }
    }
}