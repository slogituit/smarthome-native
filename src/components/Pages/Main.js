import React, { Component } from 'react';
import {createMaterialTopTabNavigator} from 'react-navigation';
import { Text } from 'react-native';
import Devices from './Devices';
import Scenes from './Scenes';
import Lights from './Lights';

const tabOptions = {    
    tabBarOptions: {
        activeTintColor:'white',
        inactiveTintColor:'white',
        style:{
            backgroundColor:'#223E4A',
         
        },
        indicatorStyle: {
            backgroundColor: 'white',
        },
    
    },
}
export  class Main extends Component{
    constructor(props) {
        super(props);
        this.state = {
          user:''
        }
    }
    render() {
        var user = this.props.navigation.state.params.user;
        this.setState({"user":user});
        return(
            <Text>MainScreen </Text>
        )
    }
}

export default TabNav = createMaterialTopTabNavigator ({
    LIGHTS: { 
        screen: Lights,
        navigationOptions: {
         tabBarLabel: 'LIGHTS',
        
        }
     },
    DEVICES: { 
        screen: Devices,
        navigationOptions: {
         tabBarLabel: 'DEVICES',
        
        }
     },
      SCENES: { 
        screen: Scenes ,
        navigationOptions: {
         tabBarLabel: 'SCENES',
       
        }
     }
},{
    tabBarPosition: 'top',
    lazy: false,
    tabBarOptions: {
        activeTintColor:'white',
        inactiveTintColor:'white',
        style: {backgroundColor:'#223E4A', padding:0},
       
        indicatorStyle: {
            backgroundColor: 'white',
        },
        labelStyle: {
            paddingTop:'10%',
            fontSize: 13,
            fontWeight: 'bold',
            width:'130%',
          },
      },
   

  },tabOptions);