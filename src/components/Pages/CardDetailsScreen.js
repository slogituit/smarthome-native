import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, BackHandler, TouchableOpacity } from 'react-native';
import { Column as Col, Row } from 'react-native-flexbox-grid';
import axios from 'axios';
import Slider from "react-native-slider";
import Toast from 'react-native-simple-toast';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default class CardDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lightFlag: false,
      body: '',
      value: 10,
      onIcon: '',
      offIcon: '',
      scrubberFlag: false,
      status: ''
    }
    this.toggleLight = this.toggleLight.bind(this);
  }

  componentDidMount() {
    let that = this;
    let title = this.props.navigation.state.params.title;
    let applianceType = this.props.navigation.state.params.applianceType;
    let onIcon = this.props.navigation.state.params.onIcon;
    if (applianceType === "light") {
      that.setState({ scrubberFlag: true })
    }
    let offIcon = this.props.navigation.state.params.offIcon;
    that.setState({ onIcon: onIcon, offIcon: offIcon });
    axios.post("https://ai.logituit.com/homeautomation/command/getState", body = {
      "userId": "amzn1.ask.account.AFYBGSJPFR6GOJV2245WBS6SWRCJP555F7Z2T52LIOP2S673EJWTNPERP2QB2M4PYDVOU5CN4RRQUMH3FI7MV3KRH6DSNGF3E7KLEHAIXV4DY6HP5FBANBEMFOAX62TOABHNYQ5QQJYBNGNQGCCOXRLSRNRRDG6XUBATWC4ARSO3CVWVSMVS6Y4YLXMQB2GNJDVHGRCMODOU27Q",
      "deviceId": "amzn1.ask.device.AFZSTSSH2X5KG2TFPDWO5YBLPQ3T6TDXJ5AXX2RQ65WFTKIH4WJRUBIF2ZUXPDZSTJYBUR3PSMXBLCNTQ5WRTZJPNOLJ4TWO2XW46ILZMCMXE4DS2OADI4YUGTLA2UUZAP5XSYV5CPOHQPDZKBX6ZSCPVUUSWOWGGPDIZTKPTESPWQLJZ4RSY",
      "appliance": title,
      "command": "dim"
    })
      .then(function (response) {
        console.log("response in dim:::::::", response);
        that.setState({ status: response.data.applianceState })
        if (!response.data.success || response.data.applianceState === "ERROR" || response.data.applianceState === "Error connecting to knx.") {
          Toast.show("Unable to connect to the server right now");
        }
        let dim = parseInt(response.data.applianceState);
        that.setState({ value: dim });
        if (dim >= 10 && response.data.success && (response.data.applianceState != "ERROR" && response.data.applianceState != "Error connecting to knx.")) {
          that.setState({ lightFlag: true });
        }
      })

    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack();
    return true;
  }

  toggleLight() {
    let that = this;
    let title = this.props.navigation.state.params.title;
    let percentage = this.state.value;

    if (title === "Blind" || title === "Screen") {

      if (that.state.lightFlag) {
        axios.post("https://ai.logituit.com/homeautomation/command/", body = {
          "userId": "amzn1.ask.account.AFYBGSJPFR6GOJV2245WBS6SWRCJP555F7Z2T52LIOP2S673EJWTNPERP2QB2M4PYDVOU5CN4RRQUMH3FI7MV3KRH6DSNGF3E7KLEHAIXV4DY6HP5FBANBEMFOAX62TOABHNYQ5QQJYBNGNQGCCOXRLSRNRRDG6XUBATWC4ARSO3CVWVSMVS6Y4YLXMQB2GNJDVHGRCMODOU27Q",
          "deviceId": "amzn1.ask.device.AFZSTSSH2X5KG2TFPDWO5YBLPQ3T6TDXJ5AXX2RQ65WFTKIH4WJRUBIF2ZUXPDZSTJYBUR3PSMXBLCNTQ5WRTZJPNOLJ4TWO2XW46ILZMCMXE4DS2OADI4YUGTLA2UUZAP5XSYV5CPOHQPDZKBX6ZSCPVUUSWOWGGPDIZTKPTESPWQLJZ4RSY",
          "appliance": title,
          "command": "close"
        })
          .then(function (response) {
            console.log("response in close Blind or Screen:::::::::::",response);
            if (that.state.status != "ERROR" && that.state.status != "Error connecting to knx.") {
              that.setState({ lightFlag: false });
              Toast.show(title + ' close successfully')
            }
          })
      }
      else {
        axios.post("https://ai.logituit.com/homeautomation/command/", body = {
          "userId": "amzn1.ask.account.AFYBGSJPFR6GOJV2245WBS6SWRCJP555F7Z2T52LIOP2S673EJWTNPERP2QB2M4PYDVOU5CN4RRQUMH3FI7MV3KRH6DSNGF3E7KLEHAIXV4DY6HP5FBANBEMFOAX62TOABHNYQ5QQJYBNGNQGCCOXRLSRNRRDG6XUBATWC4ARSO3CVWVSMVS6Y4YLXMQB2GNJDVHGRCMODOU27Q",
          "deviceId": "amzn1.ask.device.AFZSTSSH2X5KG2TFPDWO5YBLPQ3T6TDXJ5AXX2RQ65WFTKIH4WJRUBIF2ZUXPDZSTJYBUR3PSMXBLCNTQ5WRTZJPNOLJ4TWO2XW46ILZMCMXE4DS2OADI4YUGTLA2UUZAP5XSYV5CPOHQPDZKBX6ZSCPVUUSWOWGGPDIZTKPTESPWQLJZ4RSY",
          "appliance": title,
          "command": "open"
        })
          .then(function (response) {
            console.log("response in open Blind or Screen:::::::::::",response);
            if (that.state.status != "ERROR" && that.state.status != "Error connecting to knx.") {
              that.setState({ lightFlag: true });
              Toast.show(title + " is open")
            }
          })
      }
    }
    else {
      if (that.state.lightFlag) {
        axios.post("https://ai.logituit.com/homeautomation/command/", body = {
          "userId": "amzn1.ask.account.AFYBGSJPFR6GOJV2245WBS6SWRCJP555F7Z2T52LIOP2S673EJWTNPERP2QB2M4PYDVOU5CN4RRQUMH3FI7MV3KRH6DSNGF3E7KLEHAIXV4DY6HP5FBANBEMFOAX62TOABHNYQ5QQJYBNGNQGCCOXRLSRNRRDG6XUBATWC4ARSO3CVWVSMVS6Y4YLXMQB2GNJDVHGRCMODOU27Q",
          "deviceId": "amzn1.ask.device.AFZSTSSH2X5KG2TFPDWO5YBLPQ3T6TDXJ5AXX2RQ65WFTKIH4WJRUBIF2ZUXPDZSTJYBUR3PSMXBLCNTQ5WRTZJPNOLJ4TWO2XW46ILZMCMXE4DS2OADI4YUGTLA2UUZAP5XSYV5CPOHQPDZKBX6ZSCPVUUSWOWGGPDIZTKPTESPWQLJZ4RSY",
          "appliance": title,
          "command": "off"
        })
          .then(function (response) {
            console.log("in off light  response---------------", response);
            if (that.state.status != "ERROR" && that.state.status != "Error connecting to knx.") {
              that.setState({ lightFlag: false });
              Toast.show(title + ' off successfully')
            }
          })
      }
      else {
        axios.post("https://ai.logituit.com/homeautomation/command/", body = {
          "userId": "amzn1.ask.account.AFYBGSJPFR6GOJV2245WBS6SWRCJP555F7Z2T52LIOP2S673EJWTNPERP2QB2M4PYDVOU5CN4RRQUMH3FI7MV3KRH6DSNGF3E7KLEHAIXV4DY6HP5FBANBEMFOAX62TOABHNYQ5QQJYBNGNQGCCOXRLSRNRRDG6XUBATWC4ARSO3CVWVSMVS6Y4YLXMQB2GNJDVHGRCMODOU27Q",
          "deviceId": "amzn1.ask.device.AFZSTSSH2X5KG2TFPDWO5YBLPQ3T6TDXJ5AXX2RQ65WFTKIH4WJRUBIF2ZUXPDZSTJYBUR3PSMXBLCNTQ5WRTZJPNOLJ4TWO2XW46ILZMCMXE4DS2OADI4YUGTLA2UUZAP5XSYV5CPOHQPDZKBX6ZSCPVUUSWOWGGPDIZTKPTESPWQLJZ4RSY",
          "appliance": title,
          "command": "on"
        })
          .then(function (response) {
            console.log("in on light response--------------", response);
            if (that.state.status != "ERROR" && that.state.status != "Error connecting to knx.") {
              that.setState({ value: 100, lightFlag: true });
              Toast.show(title + " is on")
            }
          })
      }
    }
  }

  getVal(value) {
    let that = this;
    let title = this.props.navigation.state.params.title;
    axios.post("https://ai.logituit.com/homeautomation/command/", body = {
      "userId": "amzn1.ask.account.AFYBGSJPFR6GOJV2245WBS6SWRCJP555F7Z2T52LIOP2S673EJWTNPERP2QB2M4PYDVOU5CN4RRQUMH3FI7MV3KRH6DSNGF3E7KLEHAIXV4DY6HP5FBANBEMFOAX62TOABHNYQ5QQJYBNGNQGCCOXRLSRNRRDG6XUBATWC4ARSO3CVWVSMVS6Y4YLXMQB2GNJDVHGRCMODOU27Q",
      "deviceId": "amzn1.ask.device.AFZSTSSH2X5KG2TFPDWO5YBLPQ3T6TDXJ5AXX2RQ65WFTKIH4WJRUBIF2ZUXPDZSTJYBUR3PSMXBLCNTQ5WRTZJPNOLJ4TWO2XW46ILZMCMXE4DS2OADI4YUGTLA2UUZAP5XSYV5CPOHQPDZKBX6ZSCPVUUSWOWGGPDIZTKPTESPWQLJZ4RSY",
      "appliance": title,
      "command": "dim",
      "percentage": value
    }).then(function (response) {
      that.setState({ lightFlag: true });
      Toast.show("Brightness is " + value);
    })

  }

  render() {
    let onimg = { uri: (this.state.onIcon) }
    let offimg = { uri: (this.state.offIcon) }
    const { goBack } = this.props.navigation;
    return (
      <View style={{ height: '100%' }}>
        <View style={{ backgroundColor: '#223E4A' }}>

          <TouchableOpacity onPress={() => goBack()} style={{ height: '18%', left: '2%', width: '8%', top: '20%' }}>
            <Image source={require("../../utils/images/back.png")} style={{ height: '100%', width: '100%' }} />
          </TouchableOpacity>
          <Text style={{ color: '#fff', left: '15%', fontWeight: 'bold', top: '5%' }}>{this.props.navigation.state.params.title}</Text>
        </View>
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', backgroundColor: '#223E4A' }}>
          <View style={{ height: "100%", width: "100%", top: '30%', alignItems: 'center' }}>
            {!this.state.lightFlag ?
              <View style={{ height: 150, width: 150 }}>
                <TouchableOpacity onPress={this.toggleLight} >
                  <Image source={offimg} resizeMode='cover' style={{ height: '100%', width: '100%' }} />
                </TouchableOpacity>
              </View>
              :
              <View>
                {this.state.scrubberFlag &&
                  <View style={{ height: '100%', width: '100%' }}>
                    <Row>
                      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                        <View style={{ height: 150, width: 150, Left: '100%' }}>
                          <TouchableOpacity onPress={this.toggleLight} >
                            <View style={{
                              borderBottomLeftRadius: 74,
                              borderBottomRightRadius: 74,
                              borderTopLeftRadius: 74,
                              borderTopRightRadius: 74, overflow: 'hidden', resizeMode: 'cover'
                            }}>
                              <Image source={onimg} style={{ height: '100%', width: '100%', opacity: (this.state.value / 100) }} />
                            </View>

                          </TouchableOpacity>
                        </View>
                      </View>
                    </Row>

                    <Row marginTop={20}>
                      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }} >
                        <Image source={require("../../utils/images/bulb.png")} style={{ height: 30, width: 30, marginRight: 10 }} />
                        <Slider
                          value={this.state.value}
                          onValueChange={value => this.setState({ value: value })}
                          onSlidingComplete={value => this.getVal(value)}
                          // onSlidingComplete={value => this.state.value}
                          maximumTrackTintColor='grey'
                          minimumTrackTintColor='#ffffff'
                          minimumValue={10}
                          maximumValue={100}
                          trackStyle={{ height: 10, borderRadius: 5 }}
                          thumbTintColor='#fff44f'
                          style={{ width: 200 }}
                          step={10}
                          disabled={this.state.disabled}
                        />
                        <Image source={require("../../utils/images/bulb1.png")} style={{ height: 30, width: 30, marginLeft: 10 }} />
                      </View>
                    </Row>
                  </View>
                }
                {!this.state.scrubberFlag &&
                  <View style={{ height: 150, width: 150 }}>
                    <TouchableOpacity onPress={this.toggleLight} >
                      <Image source={onimg} resizeMode='cover' style={{ height: '100%', width: '100%' }} />
                    </TouchableOpacity>
                  </View>
                }
              </View>
            }
          </View>
        </View>
      </View>
    )
  }
}