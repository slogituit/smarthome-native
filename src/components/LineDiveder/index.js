import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {styles} from '../../styles/languageStyle';


export default class LineDiveder extends Component{
    render(){
        return (
        	<View style={styles.bgColor}>       	
                <View style={{flexDirection: 'row'}}>
                        <View style={styles.lineStyle} />                    
            	</View>
            </View>   
        );
    }
}

