import React, { Component } from 'react';
import Devices from '../components/Pages/Devices';
import Scenes from '../components/Pages/Scenes';
import { View, Text } from 'react-native';
import HeaderScreen from '../components/Pages/HeaderScreen';

export const TAB_NAVIGATION_MENU = {
    DEVICES: {
        screen: Devices,
        navigationOptions: {
            tabBarLabel: 'DEVICES',
        }
    },
    SCENES: {
        screen: Scenes,
        navigationOptions: {
            tabBarLabel: 'SCENES',
        }
    }
}
export const TAB_BAR_OPTIONS = {
    style: {
        backgroundColor: 'darkblue',
    },
    indicatorStyle: {
        backgroundColor: 'white',
        height: 2
    },

    labelStyle: {
        fontSize: 10,
        fontFamily: 'Roboto',
        color: "#fff",

    }
}